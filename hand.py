#-*- coding: utf-8 -*-
#!/usr/bin/env python


def make_rect(user,width,height,maps,cell_len):
    x = user.cellx
    y = user.celly

    if x > len(maps[0]) / 2:
        dx = -(width + 20)            
        if y > len(maps) / 2:
            dy = -(height - 60)
        else:
            dy = -10
            
    else:
        dx = width - 100
        if y > len(maps) / 2:
            dy = -(height - 60)
        else:
            dy = -10
        
    rect = Rect(x * cell_len + dx,y * cell_len,width,height)
    return rect


def title_handler(event):
    

def field_handler(event):


def cmd_handler(event):
    

def action_handler(event):


def end_handler(event):
