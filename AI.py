# -*- coding: utf-8 -*-
#!/usr/bin/env python

import masu
import chara

#任意の歩数後の移動座標を取り出す
def move_end(movelist,end):
    endlist = []
    for move in movelist:
        if move[2] == end:
            endlist.append(move[0])

    return endlist
