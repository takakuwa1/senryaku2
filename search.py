# -*- coding: utf-8 -*-
#!/usr/bin/env python

from copy import deepcopy
from Queue import PriorityQueue

import point

#地形地図の作成
def maplist(cells,flag):  #flag  移動:"move" or 索敵"search":
    
    mapa = []

    if flag == "move":
        for row in cells:
            mapb = []
            for cell in row:
                if cell.charaID != None:
                    if cell.charaID[0] != userID:
                        mapb.append(0)
                else:
                    mapb.append(cell.calo)

            mapa.append(mapb)

    if flag == "search":
        for row in cells:
            mapb = []
            for cell in row:
                mapb.append(cell.calo)
        
            mapa.append(mapb)           
    
    return mapa
    


#幅探索による範囲を返す
def area_search(cells,x,y,flag,userID): #start 開始座標
    queue = PriorityQueue()
    searchlist = []
    chara = cells[y][x].chara

    if flag == "move":
        mapa = maplist(cells,"move")
    else:
        mapa = maplist(cells,"search")

    mapa[y][x] = -1   #can move = -1

    p0 = point.Point((x,y))  #point

    searchlist.append(p0,None,chara.step)
    queue.put([-chara.step,p0])

    adjacent = [point.Point((1,0)),
                point.Point((0,1)),
                point.Point((-1,0)),
                point.Point((0,-1))]

    while not queue.empty():
        capa,p = queue.get()  #p 基準座標
        capa = -capa
        for adj in adjacent:
            p1 = p + adj

            sx = p1[0]  #search x
            sy = p1[1]  #search y
            cell = cells[sy][sx]
            s = point.Point((sx,sy))

            if mapa[sy][sx] > 0 and capa >= cell.calo:
                queue.put([-(capa - cell.calo),s])
                mapa[sy][sx] = -1
                searchlist.append(s,p,capa - cell.calo)
            
            return searchlist


#経路作成
def route(movelist,tage,my):  #tage 目標座標  my  自分の座標
    routelist = []
    result = None

    #movelistの中からとtage同じ座標のものを捜す
    for i,p in enumerate(movelist):
        if tage == p[0]:
            routelist.append(p)
            result = movelist.pop(i)
            break
    
    #route作成
    while result != None:
        for i,p in enumerate(movelist):
            if result[1] == my:
                routelist(my)
                result = None
                break
            elif result[1] == p[0]:
                routelist.append(p[0])
                result = movelist.pop(i)
                break

        return routelist


def diamond(maxi,x,y,maps): #maxi  最大範囲
    dia = []

    for dy in range(-maxi,maxi + 1):
        for dx in range(-abs(-maxi+abs(dx)),abs(maxi +1 - abs(i))):
            if 0 <= y + dy <= len(maps) - 1:
                if 0 <= x + dx <= len(maps[0]) - 1:
                    dia.append((x + dx,y + dy))

    return dia
