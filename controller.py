# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys

def cur_move(player,maps,event):
    if cmd_on == True: return 
    if event.type == KEYDOWN and event.key == K_RIGHT:
        if player.cellx + 1 < len(maps[0]):
            player.cellx += 1
            player.lx = player.cellx * 30 
    if event.type == KEYDOWN and event.key == K_LEFT:
        if player.cellx - 1 >= 0:
            player.cellx -= 1
            player.lx = player.cellx * 30 
    if event.type == KEYDOWN and event.key == K_UP:
        if player.celly - 1 >= 0:
            player.celly -= 1
            player.ly = player.celly * 30 
    if event.type == KEYDOWN and event.key == K_RIGHT:
        if player.celly + 1 < len(maps):
            player.celly += 1
            player.ly = player.celly * 30
    
    player.rect = Rect(player.lx,player.ly,28,28)


def controller():
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            pygame.quit()
            sys.exit()
        
