# -*- coding: utf-8 -*-
#!/usr/bin/env python


class Cell:
    def __init__(self,cellx,celly,length,terrain):
        self.cellx = cellx
        self.celly = celly
        self.length = length
        self.terrain = terrain  #mapchipのindexと移動消費のlist
        self.calo = self.terrain[1]
        self.chara = None


#groundmapの中身をterrainの要素に書き換える
def create_groundmap(terrain,groundmap):
    for i,row in enumerate(groundmap):
        for j,p in enumerate(row):
            groundmap[i][j] = terrain[p]

    return groundmap

#celllistを作る
def clist(groundmap,charalist):
    cs1 = []

    for y,row in enumerate(groundmap):
       cs2 = []
       for x,p in enumerate(row):
           cs2.append(cell(x,y,p))
        cs1.append(cs2)

    return cs1
