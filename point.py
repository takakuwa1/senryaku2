# -*- coding: utf-8 -*-
#!/usr/bin/env python

class Point(tuple):
    def __add__(p0,p1):
        return Point(i0 + i1 for i0,i1 in zip(p0,p1))

    def __neg__(p):
        return Point(-i for i in p)

    def __sub__(p0,p1):
        return p0 + (-p1)

    def __getattr__(self,attr):
        if attr == "x":
            return self[0]
        
        elif attr == "y":
            return self[1]

        else:
            tuple.__getattr__(self, attr)

    def dist(p):
        return abs(p.x) + abs(p.y)

    def __repr__(self):
        return "point(%s)" % tuple.__repr__(self)
