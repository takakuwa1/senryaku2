# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys
import math

import point
import search

class Character:
    def __init__(self,cellx,celly,hp,atk,defe,step,weapon,
                 chara_ID,player_ID,imagelist_index):
        self.cellx = cellx
        self.celly = celly 
        self.lx = self.cellx * 30
        self.ly = self.celly * 30
        self.hp = hp
        self.atk = atk
        self.defe = defe
        self.step = step
        self.weapon = weapon
        self.chara_ID = chara_ID
        self.player_ID = player_ID
        self.do = "move"
        self.imagelist_index = imagelist_index
        self.status = status
        self.visible = True
        
    def move(self,tag,cells,anime,userID):
        if cells[self.celly][self.cellx].chara == None and self.esc == 2:
            movelist = search.area_search(cells,self.x,self.y,
                                          "move",userID)

            for move in movelist:
                if move[0] == tage:
                    routelist = search.route(movelist,tage,(self.cellx,self.celly))
                    anime.doing = "move"
                    anime.cycle = 4
                    self.ecs -= 1

    def end(self):
        self.esc = 0

    

class Player:
    def __init__(self,ID):
        self.ID = ID
        self.cellx = 0
        self.celly = 0
        self.lx = 0
        self.ly = 0
        self.rect = Rect(self.lx,self.ly,28,28)
        
