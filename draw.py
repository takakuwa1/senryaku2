# -*- coding: utf-8 -*-
#!/usr/bin/env python

import pygame
from pygame.locals import *
import sys

sample_screen = pygame.display.set_mode((900,700))

def material(x,y,width,height,filename,colorkey = None):
    image = pygame.image.load(filename).convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey,RLEACCEL)
    return image.subsurface((x,y,width,height))


class Draw:
    def __init__(self,mapchip,screen,cells):
        self.mapchip = mapchip
        self.screen = screen
        self.cells = cells
        self.length = self.cells[0][0].length

    #地形の表示
    def map_draw(self):
        for i,row in enumerate(self.cells):
            for j,p in enumerate(row):
                mapchip_index = p.terrain[0]
                self.screen.blit(mapchip[p.mapchip_index],
                                 (j * self.length.,
                                  i * self.length))
    #charaの表示
    def chara_draw(self,charalist,charaimagelist):
        for player_ID,chara_box in enumerate(charalist):
            for chara in chara_box:
                if chara != None and chara.visible == True:
                    image = charaimagelist[chara.imagelist_index]
                    self.screen.blit(image,(chara.lx,chara.ly))

    #cursorの表示
    def cur_draw(self,user):
        pygame.draw.rect(self.screen,(255,255,255),user.rect,
                         self.cell.length / 6)
    #移動範囲の表示
    def move_draw(self,movelist,doing):
        if doing == "move": return
        for move in movelist:
            go = move[0]
            pygame.draw.rect(self.screen,(0,0,255),
                             Rect(1+ move[0] * self.length,
                                  1+ move[1] * self.length,
                                  self.length-2,self.length-2))
    
    #攻撃範囲の表示
    def attack_draw(self,cells,atklist,not_atklist,doing):
        if doing == "attack": return
        for i,atk in enumerate(atlist):
            atk_x = atk[0]
            atk_y = atk[1]
            pygame.draw.rect(self.screen,(255,0,0),
                             Rect(1+ atk_x * self.length,
                                  1+ atk_y * self.length,
                                  self.length-2,self.length-2))

        for j,natk in enumerate(not_atklist):
            natk_x = natk[0]
            natk_y = natk[1]
            mapchip_index = cells[natk_y][natk_x].terrain[0]
            self.screen.blit(self.mapchip[mapchip_index],
                             (natk_x * self.length,
                              natk_y * self.length))

    #statusの表示
    def chara_status_draw(self,user,cells,font,statusfont,maps):
        #statusfont font{hp,atk,...}
        x = user.cellx 
        y = user.celly
        chara = cells.chara
        if chara == None: return 
        for i,item in enumerate(chara.status):
            sx = 0
            sy = self.length * len(mpas) + 25 * i
            self.screen.blit(statusfont[i],(sx,sy))

            if i == 0:
                current_hp = item[1]
                max_hp = item[0]
                f1 = font.render(str(current_hp),True,
                                 (255,255,255))
                f2 = font.render("/",True,(255,255,255))
                f3 = font.render(str(max_hp),True,(255,255,255))

                self.screen.blit(f1,(x + 40,y))
                self.screen.blit(f2,(x + 70,y))
                self.screen.blit(f3,(x + 85,y))
                
            elif i == 3:
                f1 = font.render(str(item[0]+1),True,(255,255,255))
                f2 = font.render("~",True,(255,255,255))
                f3 = font.render(str(item[1]),True,(255,255,255))

                self.screen.blit(f1,(x + 100,y))
                self.screen.blit(f1,(x + 115,y))
                self.screen.blit(f1,(x + 130,y))
            
            elif i == 4:
                f1 = font.render(str(item),True,(255,255,255))

                self.screen.blit(x + 80,y)
    
            else:
                f1 = font.render(str(item),True,(255,255,255))
                
                self.screen.blit(f1,(x + 50,y))

    #damage値の表示
    def damage_draw(self,font,maps,damage):
        x = 200
        y = self.length * len(maps)
        
        dmg = font.render("damage:",True,(255,255,255))
        self.screen.blit(dmg,(x,y))

        x = 290
        dmg1 = font.render(str(damage),True,(255,255,255))
        self.screen.blit(dmg1,(x,y))

    #所属の表示
    def ID_draw(self,charalist):
        for chara_box in charalist:
            for chara in chara_box:
                if chara != None:
                    x = chara.lx
                    y = chara.ly
                    if chara.player_ID == 0:
                        pygame.draw.rect(self.screen,(255,255,0),
                                         Rect(x,y,10,10))
                    elif chara.chara_ID == 1:
                        pygame.draw.rect(self.screen,(255,0,255),
                                         Rect(x,y,10,10))
    #charaの状態表示
    def doing_draw(self,charalist):
        for chara_box in charalist:
            for chara in chara_box:
                if chara != None:
                    x = chara.lx + 20
                    y = chara.ly
                    if chara.do == "end":
                        pygame.draw.rect(self.screen,(128,128,128),
                                         Rect(x,y,10,10))
                    elif chara.do == "attack":
                        pygame.draw.rect(self.screen,(255,255,0),
                                         Rect(x,y,10,10))
                    elif chara.do == "move":
                        pygam.draw.rect(self.screen,(0,0,255),
                                        Rect(x,y,10,10))

    #command window
    def cmd_draw(self,rect,cmdfont,row):
        inner_rect = rect.inflate(-10,-10)
        text_rect = rect.inflate(-30,-30)
        pygame.draw.rect(self.screen,(255,255,255),rect)
        pygame.draw.rect(self.screen,(0,0,0),inner_rect)

        for cmd in cmdfont:
            dx = text_rect[0]
            dx = text_rect[0]
            self.screen.blit(cmd,(dx,dy))
            
        curx = text_rect[0]
        cury = text_rect[1] + 18 + 40 * row
        
        pygame.draw.rect(self.screen,(255,255,255),
                         Rect(curx,cury,10,10))

        
    def title_draw(self,titlefont,stagefont,row):
        self.screen.blit(titlefont,(200,200))
        for i,stage in enumerate(stagefont):
            self.screen.blit(stage,(250,200 + i * 30))

        #cursor
        curx = 220
        cury = 210 + row * 30
        pygame.draw.rect(self.screen,(255,255,255),
                         Rect(curx,cury,10,10))

    def end_draw(self,endfont):
        self.screen.blit(endfont,(250,200))
        
